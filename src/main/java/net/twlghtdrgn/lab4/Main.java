package net.twlghtdrgn.lab4;

import net.twlghtdrgn.lab4.with_factory.Box;
import net.twlghtdrgn.lab4.with_factory.fruits.Apple;
import net.twlghtdrgn.lab4.with_factory.fruits.Banana;
import net.twlghtdrgn.lab4.with_factory.fruits.Fruit;
import net.twlghtdrgn.lab4.with_factory.fruits.Orange;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {


        Box appleBox1 = new Box("Apple");
        Box appleBox2 = new Box("Apple");
        Box orangeBox = new Box("Orange");
        Box bananaBox1 = new Box("Banana");


//        appleBox1.check(new Apple());

        appleBox1.add(new Apple());
        appleBox1.add(new Apple());

        appleBox2.add(new Apple());
        appleBox2.add(new Apple());

        orangeBox.add(new Orange());
        orangeBox.add(new Orange());

        bananaBox1.add(new Banana());

//        orangeBox.getFruits();


        System.out.println(appleBox1.getWeight());
        System.out.println(appleBox2.getWeight());
        System.out.println(orangeBox.getWeight());
        System.out.println(bananaBox1.getWeight());

        System.out.println(appleBox1.compare(appleBox2));
        System.out.println(orangeBox.compare(appleBox2));

        bananaBox1.add(new Apple());
        bananaBox1.getFruits();

        appleBox2.add(new Orange());
        appleBox2.getFruits();

//        appleBox1.getFruits();
//        appleBox2.getFruits();
//        orangeBox.getFruits();
//        bananaBox1.getFruits();

        appleBox2.pourOver(appleBox1);
        appleBox2.pourOver(bananaBox1);

        bananaBox1.pourOver(orangeBox);
        orangeBox.pourOver(bananaBox1);
        appleBox1.getFruits();
        System.out.println("-------------");
        bananaBox1.getFruits();

    }
}
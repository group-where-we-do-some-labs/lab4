package net.twlghtdrgn.lab4.with_factory.fruits;

public class Banana extends Fruit {
    public final float WEIGHT = 2.0f;
    @Override
    public Float getWeight() {
        return WEIGHT;
    }

    @Override
    public String getType() {
        return "Banana";
    }
}


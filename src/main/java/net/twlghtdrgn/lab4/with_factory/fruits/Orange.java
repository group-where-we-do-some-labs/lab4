package net.twlghtdrgn.lab4.with_factory.fruits;

public class Orange extends Fruit {
    public final float WEIGHT = 1.5f;
    @Override
    public Float getWeight() {
        return WEIGHT;
    }

    @Override
    public String getType() {
        return "Orange";
    }
}

package net.twlghtdrgn.lab4.with_factory.fruits;

public class Apple extends Fruit {
    public final float WEIGHT = 1.0f;
    @Override
    public Float getWeight() {
        return WEIGHT;
    }

    @Override
    public String getType() {
        return "Apple";
    }
}

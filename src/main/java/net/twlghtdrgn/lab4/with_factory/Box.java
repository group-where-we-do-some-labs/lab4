package net.twlghtdrgn.lab4.with_factory;

import net.twlghtdrgn.lab4.ex.box.BoxX;
//import net.twlghtdrgn.lab4.ex.fruits.Fruit;
import net.twlghtdrgn.lab4.ex.fruits.Orange;
import net.twlghtdrgn.lab4.with_factory.fruits.Fruit;
import net.twlghtdrgn.lab4.with_factory.listForBoxFactory.AppleListFactory;
import net.twlghtdrgn.lab4.with_factory.listForBoxFactory.FruitListFactory;
import net.twlghtdrgn.lab4.with_factory.listForBoxFactory.ListFactory;
import net.twlghtdrgn.lab4.with_factory.listForBoxFactory.OrangeListFactory;

import java.util.ArrayList;
import java.util.List;

public class Box {

    ListFactory listFactory;
    List<Fruit> fruits;
    String type;

    public Box(String type) {
        listFactory = createListByType(type);
        fruits = listFactory.createList();
        this.type = type;

    }

    static ListFactory createListByType(String type) {
        if (type.equals("Apple")) {
            return new AppleListFactory();
        } else if (type.equals("Orange")) {
            return new OrangeListFactory();
        } else if (type.equals("Banana")) {
            return new FruitListFactory();
        }else {
            throw new RuntimeException(type + " is unknown.");
        }
    }

    public void getFruits() {
         for (Fruit t : this.fruits) {
             System.out.println(t.getType());
         }
    }

    public void check(Fruit fruit) {
        System.out.println(this.type);
        System.out.println(fruit.getType());
        System.out.println(this.type.equals(fruit.getType()));
    }

    public void add(Fruit fruit) {
//        if (this.type.equals("Banana") || this.type.equals(fruit.getType()))
//            fruits.add(fruit);
//        else System.out.println("Incorrect type of fruit");
        this.fruits.add(fruit);
    }

    public float getWeight(){
        float weight = 0;
        if (!this.fruits.isEmpty())
            for (Fruit t : this.fruits) weight += t.getWeight();
        return weight;
    }

    public boolean compare(Box box) {
        return this.getWeight() == box.getWeight();
    }

//    public void changeTypeOfList(String type) {
//
//    }

    public void pourOver(Box box) {
        if (!this.fruits.isEmpty()) {
            int size = this.fruits.size() - 1;
            if (box.type.equals("Banana") || this.type.equals(box.type))
                while (!this.fruits.isEmpty()) {
                    box.fruits.add(this.fruits.get(size));
                    this.fruits.remove(size);
                    size--;
                }
            else System.out.println("Mismatch type of fruits in Boxes ");
        } else System.out.println("Box is empty");
    }
}

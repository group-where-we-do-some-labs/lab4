package net.twlghtdrgn.lab4.with_factory.listForBoxFactory;

import net.twlghtdrgn.lab4.ex.fruits.Apple;

import java.util.ArrayList;
import java.util.List;

public class AppleListFactory implements ListFactory{
    @Override
    public List createList() {
        return new ArrayList<Apple>();
    }
}

package net.twlghtdrgn.lab4.with_factory.listForBoxFactory;

import net.twlghtdrgn.lab4.ex.fruits.Fruit;

import java.util.ArrayList;
import java.util.List;

public class FruitListFactory implements ListFactory{
    @Override
    public List createList() {
        return new ArrayList<Fruit>();
    }
}

package net.twlghtdrgn.lab4.with_factory.listForBoxFactory;

import java.util.List;

public interface ListFactory {
    List createList();
}

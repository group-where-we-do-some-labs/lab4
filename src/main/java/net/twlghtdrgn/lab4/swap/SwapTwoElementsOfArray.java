package net.twlghtdrgn.lab4.swap;

public class SwapTwoElementsOfArray<T> {

    void swap(T[] array, int firstE, int secondE) {
        T t;
        t = array[firstE];
        array[firstE] = array[secondE];
        array[secondE] = t;
    }
}

package net.twlghtdrgn.lab4.swap;

public class MyMain {
    public static void main(String[] args) {

        Object[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        SwapTwoElementsOfArray<Object> sw = new SwapTwoElementsOfArray<>();
        sw.swap(arr, 3, 8);
        for(Object o : arr) {
            System.out.println(o);
        }
    }
}

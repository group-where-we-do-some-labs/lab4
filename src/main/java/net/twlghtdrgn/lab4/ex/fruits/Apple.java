package net.twlghtdrgn.lab4.ex.fruits;

public class Apple extends Fruit{
    public final float WEIGHT = 1.0f;
    @Override
    public Float getWeight() {
        return WEIGHT;
    }

    @Override
    public String getType() {
        return "Яблоко";
    }
}

package net.twlghtdrgn.lab4.ex;

import net.twlghtdrgn.lab4.ex.box.BoxX;
import net.twlghtdrgn.lab4.ex.fruits.Apple;
import net.twlghtdrgn.lab4.ex.fruits.Banana;
import net.twlghtdrgn.lab4.ex.fruits.Fruit;
import net.twlghtdrgn.lab4.ex.fruits.Orange;

public class MyMain {
    public static void main(String[] args) {
        BoxX<Apple> appleBox1 = new BoxX<>("Apple");
        BoxX<Apple> appleBox2 = new BoxX<>("Apple");
        BoxX<Orange> orangeBox = new BoxX<>("Orange");
        BoxX<Banana> bananaBox1 = new BoxX<>("Banana");
//        BoxX<Fruit> fruit = bananaBox1;

        appleBox1.add(new Apple());
        appleBox1.add(new Apple());
        appleBox1.add(new Apple());

        appleBox2.add(new Apple());
        appleBox2.add(new Apple());

        orangeBox.add(new Orange());
        orangeBox.add(new Orange());

        bananaBox1.add(new Banana());

        System.out.println(appleBox1.getWeight());
        System.out.println(appleBox2.getWeight());
        System.out.println(orangeBox.getWeight());
        System.out.println(bananaBox1.getWeight());

        System.out.println(appleBox1.compare(appleBox2));
        System.out.println(appleBox1.compare(orangeBox));
        System.out.println(appleBox2.compare(bananaBox1));

        System.out.println("Апельсины -> Яблоки1");
        orangeBox.pourOver(appleBox1);
        System.out.println("-----------------------------------");
        System.out.println("Апельсины -> Бананы1");
        orangeBox.pourOver(bananaBox1);

        System.out.println(bananaBox1.getIn());

        System.out.println("-----------------------------------");


        System.out.println("-----------------------------------");

        System.out.println("Яблоки1 -> Яблоки2");
        appleBox1.pourOver(appleBox2);


        System.out.println("-----------------------------------");



//        bananaBox1.add(new Apple());



    }
}

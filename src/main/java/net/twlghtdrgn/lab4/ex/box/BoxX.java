package net.twlghtdrgn.lab4.ex.box;

import net.twlghtdrgn.lab4.ex.fruits.Fruit;

import java.util.ArrayList;
import java.util.List;

public class BoxX <T extends Fruit> {

    private String type;

    public BoxX(String type){
        this.type = type;
    }

    private List<T> in = new ArrayList<>();

    public List<T> getIn() {
        return in;
    }
    public void add(T fruit) {
        in.add(fruit);
    }

    public float getWeight(){
        float weight = 0;
        if (!this.in.isEmpty())
            for (T t : this.in) weight += t.getWeight();
        return weight;
    }

    public boolean compare(BoxX<?> box) {
        return this.getWeight() == box.getWeight();
    }

    public void pourOver(BoxX box) {
        if (!this.in.isEmpty()) {
            int size = this.in.size() - 1;
            if (box.type.equals("Banana") || this.type.equals(box.type))
                while (!this.in.isEmpty()) {
                    box.in.add(this.in.get(size));
                    this.in.remove(size);
                    size--;
                }
            else System.out.println("В коробках разные типы фруктов");
        } else System.out.println("Коробка пуста, из нее нечего пересыпать");
    }
}

package net.twlghtdrgn.lab4.ex.box;

import net.twlghtdrgn.lab4.ex.fruits.Fruit;

import java.util.ArrayList;
import java.util.List;

public class Box<T extends Fruit> {

    private List<T> in = new ArrayList<>();

    public List<T> getIn() {
        return in;
    }
    public void add(T fruit) {
        in.add(fruit);
    }

    public float getWeight(){
        float weight = 0;
        if (!this.in.isEmpty())
            for (int i = 0; i < this.in.size()-1; i++)
                weight += this.in.get(i).getWeight();
        return weight;
    }

    public static boolean compareClasses(Box box1, Box box2) {
        if (!box1.in.isEmpty())
            return (box1.in.get(0).getClass() == box2.getIn().get(0).getClass());
        else {
            System.out.println("Одна из коробок пуста!");
            return false;
        }
    }
    public boolean compare(Box<?> box) {
        return this.getWeight() == box.getWeight();
    }

    public void pourOver(Box box) {
            try {
                    for (int i = this.in.size()-1; i >= 0; i--) {
                        box.in.add(this.in.get(i));
                        this.in.remove(i);
                    }

                } catch (Exception e) {
                System.out.println("В коробках разные типы фруктов");
                }
    }
}

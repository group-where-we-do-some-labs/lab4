package net.twlghtdrgn.lab4.convetArrListMethod;

import java.util.ArrayList;

public class MyMain {
    public static void main(String[] args) {

        Object[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};

        ArrayList<Object> arrayList = ConvertArrayToArrayList.convert(arr);
        for (Object e: arrayList)
            System.out.println(e);
    }
}

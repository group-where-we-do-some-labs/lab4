package net.twlghtdrgn.lab4.convetArrListMethod;

import java.util.ArrayList;
import java.util.Arrays;

public class ConvertArrayToArrayList {

    static ArrayList<Object> arrayList = new ArrayList<>();

    protected static ArrayList<Object> convert(Object[] array) {
        arrayList.addAll(Arrays.asList(array));
        return arrayList;
    }

}
